<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Home</title>
</head>
<body>
<div>
    <h1>Page Home</h1>
    <a href="<s:url action="register-view" />">Go to register</a>
</div>
</body>
</html>
