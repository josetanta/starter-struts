<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <s:head/>
    <title>Register</title>
</head>
<body>
<div>
    <h1>Page Register</h1>
    <s:form action="register">
        <s:textfield name="personBean.firstname" label="First Name" required="true"/>
        <s:textfield name="personBean.lastname" label="Last Name" required="true"/>
        <s:textfield name="personBean.email" label="Email" required="true" type="email"/>
        <s:submit value="Send"/>
    </s:form>

    <p>
        <s:property value="personBean"/>
    </p>
</div>
</body>
</html>
