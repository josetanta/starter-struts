<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Register successfully</title>
</head>
<body>
<div>
    <main>
        <h3>Thank you for registering</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, atque aut consequatur deserunt dicta earum,
            error est et explicabo laborum nobis tenetur voluptatibus!</p>
        <a href="<s:url action="home" />">Return to Home</a>
        <p>
            <s:property value="personBean"/>
        </p>
    </main>
</div>
</body>
</html>
