<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Basic Struts 2 Application - Welcome</title>
</head>
<body>
<div>
    <h2>This is an app with Struts 2</h2>
</div>
<div>
    <h3>Home</h3>
    <p><a href="<s:url action="home" />">go to home</a></p>
</div>
</body>
</html>
