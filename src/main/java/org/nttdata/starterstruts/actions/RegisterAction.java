package org.nttdata.starterstruts.actions;

import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.nttdata.starterstruts.model.Person;

@Getter
@Setter
public class RegisterAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	private Person personBean;

	@Override
	public String execute() {
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (personBean.getEmail().isEmpty()) {
			addFieldError("personBean.email", "Email is required.");
		}
	}
}
