package org.nttdata.starterstruts.actions;


import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.nttdata.starterstruts.model.Person;

@Getter
@Setter
public class HomeAction extends ActionSupport {

	private Person personBean;

	@Override
	public String execute() {
		System.out.println("############## HOME Action ################");
		return SUCCESS;
	}
}
